package com.custom.mockrunner;

public class MockFactory
{
    public static MockRunner getMockRunner (String service, String environment, String resourcepath) {	
    		 
    	if (service.equalsIgnoreCase("getPR") ) {			
    		return (new JMSMockForGetPRResponse(environment,resourcepath)) ;
    	} else if (service.equalsIgnoreCase("getPO") ) {			
    		return (new JMSMockForGetPOResponse(environment, resourcepath)) ;
    	} else if (service.equalsIgnoreCase("getSR") ) {			
    		return (new JMSMockForGetSRResponse(environment, resourcepath)) ;
    	} else {
    		return null;
    	}
    }
}