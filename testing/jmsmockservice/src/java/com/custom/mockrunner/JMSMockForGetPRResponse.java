package com.custom.mockrunner;

import java.util.Properties;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import org.w3c.dom.Document;

import com.common.util.EnvironmentProperties;
import com.common.util.CommonUtil;
import com.common.util.JMSReceive;
import com.common.util.JMSSend;
import com.common.util.XMLUtil;

public class JMSMockForGetPRResponse implements MessageListener,MockRunner 
{
    private boolean quit = false;   
    String environment = "" , resourcePath = "";
    Properties prop = null;

    public JMSMockForGetPRResponse (String environment, String resourcePath) {
    	this.environment = environment ; 
    	this.resourcePath = resourcePath ;
    	this.prop = EnvironmentProperties.getProperties(environment,resourcePath); 
    }

    public void process() throws NamingException, JMSException {

	    InitialContext ic = CommonUtil.getInitialContext(prop.getProperty("SIBUS_PROVIDER_URL"),prop.getProperty("JNDI_FACTORY"));	   
	    JMSReceive qr = new JMSReceive();

	    qr.createReceiver(ic, prop.getProperty("PR_RequestQueue"), prop.getProperty("JMS_BINDING")).setMessageListener(this);;

	    System.out.println("JMS Ready To Receive Messages (To quit, send a \"quit\" message).");

	    // Wait until a "quit" message has been received.
	    synchronized(qr) {
	     while (! quit) {
	       try {
	         qr.wait();
	       } catch (InterruptedException ie) {}
	     }
	    }
	    qr.close();

    } 

    public void onMessage(Message msg)
    {
       try {
        String msgText;
        if (msg instanceof TextMessage) {
          msgText = ((TextMessage) msg).getText();
          System.out.println("Message is of Text Message Category.");
        } else {
          msgText = msg.toString();
          System.out.println("Message is converted into String.");
        }
        System.out.println("Message Received: "+ msgText );

        Document dc = XMLUtil.convertStringToDocument(msgText);

        String prID = XMLUtil.findValueInXMLUsingXPath("//PRNUM", dc) ; 
        String correlationID = XMLUtil.findValueInXMLUsingXPath("//correlationId", dc) ; 
       
        String folderName = XMLUtil.findFolderNameFromXMLMap(prID,this.resourcePath,"getPR", null ) ;
        folderName = (folderName != null) ? folderName : "PRResponseException" ;

        if (msgText.equalsIgnoreCase("quit")) {
            synchronized(this) {
              quit = true;
              this.notifyAll(); // Notify main thread to quit
            }
          } else { 
              JMSSend jmsSend = new JMSSend(correlationID,accessSeekerID,prID);

              String absoluteFilePath = CommonUtil.getAbsoluteFilePath(this.resourcePath, "getPR", folderName) ;
              System.out.println("File Path: "+ absoluteFilePath);

              jmsSend.readAndSend(prop.getProperty("SIBUS_PROVIDER_URL"),prop.getProperty("PR_ResponseQueue"),prop.getProperty("JMS_BINDING"),
        	      prop.getProperty("JNDI_FACTORY"), absoluteFilePath);
                              
          }
         } catch (Exception jmse) {
             System.err.println("An exception occurred: "+jmse.getMessage());
         }      
    }

}