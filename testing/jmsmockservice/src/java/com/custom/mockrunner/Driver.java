package com.custom.mockrunner;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

public class Driver
{      
    public static void main(String[] args) throws Exception {
	String env = null,service = null, resourcepath = ""; 
	 try {
	         // create the command line parser
	         CommandLineParser parser = new DefaultParser();

	         // create the Options (short text, long text, hasArg , description)
	         Options options = new Options();
	         options.addOption("e", "env", true, "environment to be connected");
	         options.addOption("s", "service", true, "Service name");
	         options.addOption("f", "resourcepath", true, "Resource Folder Path");

	         // parse the command line arguments
	         CommandLine line = parser.parse(options, args);

	         // validate that inputs has been set
	         if (line.hasOption("env")) {
	            env = line.getOptionValue("env").trim();
	            System.out.println("Environment:" + env);
	         }
	       
	         if (line.hasOption("service")) {
	            service = line.getOptionValue("service").trim();
	            System.out.println("Service: " + service);
	         }
	         
	         if (line.hasOption("resourcepath")) {
	            resourcepath = line.getOptionValue("resourcepath").trim();
	            System.out.println("Resource Base Path: " + resourcepath);
		 }
	         
	         MockRunner mr = MockFactory.getMockRunner(service,env,resourcepath);
	         mr.process();	         
	         
	      } catch (ParseException exp) {
	         System.out.println("Unexpected exception:" + exp.getMessage());
	         throw exp;
	      }
    }
}
