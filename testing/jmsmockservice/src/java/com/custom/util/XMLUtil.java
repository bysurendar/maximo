package com.custom.util;

import java.io.File;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLUtil
{
    public static Document convertStringToDocument(String xmlStr) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();  
        DocumentBuilder builder;  
        try  
        {  
            builder = factory.newDocumentBuilder();  
            Document doc = builder.parse( new InputSource( new StringReader( xmlStr ) ) ); 
            return doc;
        } catch (Exception e) {  
            e.printStackTrace();  
        } 
        return null;
    }
    
    public static String findValueInXMLUsingXPath(String xpathStr, Document dc) {
    	try {
	     String value = null;
	     XPathFactory xfact = XPathFactory.newInstance();
	     XPath xpath = xfact.newXPath();	
	     value = xpath.evaluate(xpathStr, dc);
	     System.out.println ("x path str " + xpathStr + " value: " + value) ;
	     return value ; 
	    }
	   catch (XPathExpressionException e) {
	     e.printStackTrace();
	    }
		 return null;
    }
    
    public static String findFolderNameFromXMLMap(String key,String resourcePath,String service, String technologyType) {
	
    	String mappingFilePath = "" ; 
    	
    	if (service.equalsIgnoreCase("getPR") ) { 
    		mappingFilePath 
			= resourcePath  + File.separator + "templates" + File.separator + "getPRResponse" + File.separator + "Mapping.xml" ; 
    	}
    	   	
    	File mappingXmlFile = new File(mappingFilePath);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
        
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc =(Document)  dBuilder.parse(mappingXmlFile);
            doc.getDocumentElement().normalize();
            
            System.out.print("Root element: ");
            System.out.println(doc.getDocumentElement().getNodeName());
            
            NodeList nodeList =  doc.getElementsByTagName("map");
            
            for (int i = 0; i < nodeList.getLength(); i++) {
                       	
        	Node node =  nodeList.item(i);
        

        	if(node.getNodeType() == Node.ELEMENT_NODE) {
        	    Node mapChildOne =  node.getChildNodes().item(1)	;       	    
        	    System.out.println ("Location Value:" + mapChildOne.getAttributes().getNamedItem("value").getNodeValue()) ;         	  
        	    String mapValue = mapChildOne.getAttributes().getNamedItem("value").getNodeValue(); 
        	         	  
        	   if ( mapValue.equals(key) ) {
        	       System.out.println ("Folder Name:" + node.getChildNodes().item(3).getTextContent()) ; 
        	       return node.getChildNodes().item(3).getTextContent() ;
        	   }       	    
        	}
        	
        }
           
        } catch (Exception e1) {
            e1.printStackTrace();
        }
	
	 return null ; 
    }  
    
    public static String replaceValues(String strToReplace, String[] sourceValueArray, String[] targetValueArray) {    	
    	
    	if (sourceValueArray.length != targetValueArray.length) { 
    		 System.out.println ("Number of values in Source Array: " + sourceValueArray.length + " is not equal to Target Array: " + 
    				 targetValueArray.length) ; 
    		 throw new ArrayIndexOutOfBoundsException("Number of values in Source Array: " + sourceValueArray.length + " is not equal to Target Array: " + 
    				 targetValueArray.length);
    	}
    	
    	for (int i=0; i < sourceValueArray.length ; i++ ) {     		    		
    		strToReplace = strToReplace.replace(sourceValueArray[i], targetValueArray[i]) ;     		
    	} 
    	return strToReplace ; 
    	
    }
    
    
}