package com.custom.util;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.naming.Context;
import javax.naming.NamingException;

public class JMSReceive
{    
    private ConnectionFactory qconFactory;   
    private Connection qcon;
    private Session qsession;
    private MessageConsumer qreceiver;
    private Queue queue;
    
	 public MessageConsumer createReceiver(Context ctx, String queueName, String jmsFactory) throws NamingException, JMSException
	 {	    
	    qconFactory = (ConnectionFactory) ctx.lookup(jmsFactory);	   
	    qcon =  qconFactory.createConnection();	   
	    qsession = qcon.createSession(false, Session.AUTO_ACKNOWLEDGE);
	    queue = (Queue) ctx.lookup(queueName);
	    qreceiver =  qsession.createConsumer(queue);	   
	    qcon.start();
	    return qreceiver ;
	 }
	 	 	
    public void close()throws JMSException
    {
       qreceiver.close();
       qsession.close();
       qcon.close();
    }
}
