package com.custom.util;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class EnvironmentProperties
{        
    public static Properties getProperties (String environment, String resourcePath) { 
	FileInputStream input = null;
	Properties propertyList = new Properties();
	try {			
	    	String propertyfileName = environment + ".properties" ;
	    	String absoluteFilePath = resourcePath + File.separator + "properties" +  File.separator + propertyfileName; 
	    	
	    	System.out.println("file path:" + absoluteFilePath);		
	    	input = new FileInputStream(absoluteFilePath) ; 
	    	
		if(input   == null){	           
			propertyList = null;		    
		} else {
		//load a properties file from class path, inside static method
			propertyList.load(input);	
		}
                    
    	} catch (IOException ex) {
    		ex.printStackTrace();
        }
	finally {
		if (input != null) {
			try {
				input.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	return propertyList;	 
    }        
}