package com.custom.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

/** This example shows how to establish a connection
* and send messages to the JMS queue. The classes in this
* package operate on the same JMS queue. Run the classes together to
* witness messages being sent and received, and to browse the queue
* for messages. The class is used to send messages to the queue.
*
* @author Copyright (c) 1999-2005 by BEA Systems, Inc. All Rights Reserved.
*/
public class JMSSend
{
     
 private Queue queue;
 private TextMessage msg;
 private ConnectionFactory qconFactory;
 private Connection qcon;
 private Session qsession;
 private MessageProducer qsender;
 
 public String prID , correlationID ; 

 public JMSSend(String correlationID,  String prID) { 
     this.correlationID = correlationID ; 
     this.prID = prID ; 
 }
 
 /**
  * Creates all the necessary objects for sending
  * messages to a JMS queue.
  *
  * @param ctx JNDI initial context
  * @param queueName name of queue
  * @exception NamingException if operation cannot be performed
  * @exception JMSException if JMS fails to initialize due to internal error
  */
 public void init(Context ctx, String queueName, String jmsFactory)
    throws NamingException, JMSException
 {
    qconFactory = (ConnectionFactory) ctx.lookup(jmsFactory);
    qcon = qconFactory.createConnection();
    qsession = qcon.createSession(false, Session.AUTO_ACKNOWLEDGE);
    queue = (Queue) ctx.lookup(queueName);
    qsender = qsession.createProducer(queue);
   
    msg = qsession.createTextMessage();
    qcon.start();
 }

 /**
  * Sends a message to a JMS queue.
  *
  * @param message  message to be sent
  * @exception JMSException if JMS fails to send message due to internal error
  */
 public void send(String message) throws JMSException {
    msg.setText(message);
    qsender.send(msg);
 }

 /**
  * Closes JMS objects.
  * @exception JMSException if JMS fails to close objects due to internal error
  */
 public void close() throws JMSException {
    qsender.close();
    qsession.close();
    qcon.close();
 } 
 
 public void readAndSend(String url, String responseQueue,String jmsFactory, String jndiFactory, String filePath) throws IOException, JMSException, NamingException, ParserConfigurationException, SAXException {
     InitialContext ic = CommonUtil.getInitialContext(url,jndiFactory);
     this.init(ic, responseQueue, jmsFactory); 
          
     File xmlFile = new File(filePath);
     // Let's get XML file as String using BufferedReader
     // FileReader uses platform's default character encoding
     // if you need to specify a different encoding, use InputStreamReader
     Reader fileReader = new FileReader(xmlFile);
     BufferedReader bufReader = new BufferedReader(fileReader);
     
     StringBuilder sb = new StringBuilder();
     String line = bufReader.readLine();
     while( line != null){
         sb.append(line).append("\n");
         line = bufReader.readLine();
     }
     String xml2String = sb.toString();
                  
     xml2String = XMLUtil.replaceValues(xml2String, 
    		 new String[] {"${prID}","${correlationID}","${timestamp}"},
    		 new String[] {this.prID,this.correlationID,CommonUtil.currentDateAsString()}) ; 
          
     System.out.println("Message as Mock response : ");
     System.out.println(xml2String);
     
     msg.setText(xml2String);
     qsender.send(msg);
     bufReader.close();
     this.close();    
 }
     
}
