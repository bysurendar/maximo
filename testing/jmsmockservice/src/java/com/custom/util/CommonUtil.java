package com.custom.util;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;
import java.util.TimeZone;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class CommonUtil
{
    public static String currentDateAsString() {
    	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    	sdf.setTimeZone( TimeZone.getTimeZone( "UTC" ) );	
    	return sdf.format( new Date() ) ;
    }  
    
    public static String getAbsoluteFilePath(String resourcePath, String service, String folderName) { 
    	//String absoluteFilePath = "" ;
    	StringBuffer absoluteFilePath = new StringBuffer(""); 
    	if (service.equalsIgnoreCase("getPRResponse")) {
    		absoluteFilePath.append(resourcePath).append(File.separator) ;
    		absoluteFilePath.append("templates").append(File.separator) ;
    		absoluteFilePath.append("PRResponse").append(File.separator) ;
    		absoluteFilePath.append(folderName).append(File.separator);
    		absoluteFilePath.append("getPRResponse.xml") ; 
    		
    	}	
    	    	
    	return absoluteFilePath.toString() ;	
    }
    
    
    public static String getAbsoluteFilePathForError(String resourcePath, String service) {    	
    	StringBuffer absoluteFilePath = new StringBuffer(""); 
    	if (service.equalsIgnoreCase("getLocation")) {
    		absoluteFilePath.append(resourcePath).append(File.separator) ;
    		absoluteFilePath.append("templates").append(File.separator) ;
    		absoluteFilePath.append("PRResponseException").append(File.separator) ;
    		absoluteFilePath.append("test").append(File.separator);
    		absoluteFilePath.append("getPRResponseException.xml") ; 
    	}	
    	
    	    	
    	return absoluteFilePath.toString() ;	
    }
              
    public static InitialContext getInitialContext(String url, String jndi_factory) throws NamingException
    {	    
       Hashtable<String, String> env = new Hashtable<String, String>();
       env.put(Context.INITIAL_CONTEXT_FACTORY, jndi_factory) ;    
       env.put(Context.PROVIDER_URL, url);
       System.out.println("Test the hash table for JNDI factory");
       return new InitialContext(env);
    }
        
}
