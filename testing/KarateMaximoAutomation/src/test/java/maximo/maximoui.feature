## Maximo Create Work Order from UI scenario
@tag
Feature: Maximo UI Test
 	Background:
    * configure driver = { type: 'chrome',  addOptions: ["--remote-allow-origins=*"],
    showDriverLog: true, executable:'C:\\Users\\Suren\\Chrome\\Application\\chrome.exe' }
# executable param PATH is set because Chrome is not installed in default path in my machine
# Karate looks for default Chrome path C:/Program Files (x86)/Google/Chrome/Application/chrome.exe

  @tag1
  Scenario: Maximo login
    Given driver 'https://hostname.maximo.com.au/maximo'
     And  delay(1000)
     And input('input[id=username]', 'maxadmin')
     And input('input[id=password]', 'maxadmin')
     When submit().click("button[id=loginbutton]") 
     Then match driver.title == 'Start Center' 
     And  retry(5).submit().click("a[id=FavoriteApp_WOTRACK]")  
     And  delay(1000)
     And match driver.title == 'Work Order Tracking' 
     And  delay(1000)
     And click("img[id=toolactions_INSERT-tbb_image]")  
     And  delay(2000)
     And input("//*[@id='m3b6a207f-tb2']",'Test Description')
     And  delay(1000)
     And input("//*[@id='m56609cbe-tb']",'CM')
     And  delay(1000)  
     And click("img[id=toolactions_SAVE-tbb_image]")  
     And  delay(3000)