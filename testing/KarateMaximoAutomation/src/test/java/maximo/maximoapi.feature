# API test is to query PERSON record GET HTTP and validate output. 
# https://<maximohostname>//maxrest/rest/os/mxperson/?personid='WILSON'&_lid=WILSON&_lpwd=password
# GET response with PERSONID field value
@tag
Feature: Validate the Maximo GET response in XML
  I want to use this template for my feature file

Background: Set up the base URL
Given url 'https://<maximohostname>' 
# <maximohostname> is domain name of xxx.maximo.com

  @tag1
  Scenario: To validate the response
  * def query = {personid:'WILSON' , _lid:'WILSON' , _lpwd:'password'} 
    Given path '/maxrest/rest/os/mxperson'
    And header Accept = 'application/xml'
    And params query
    When method get
    Then status 200
    And match response/QueryMXPERSONResponse/MXPERSONSet/PERSON/PERSONID == 'WILSON'