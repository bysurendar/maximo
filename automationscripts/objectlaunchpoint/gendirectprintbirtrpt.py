from java.util import Hashtable;
from psdi.util.logging import MXLoggerFactory
from psdi.server import MXServer
from psdi.webclient.system.controller import WebClientEvent

itemList = mbo.getString("itemNumList")

if len (itemList) > 0 :
	itemList = itemList[1:]                 # remove first extra char , 
	siteID = mbo.getString("SITEID")
	
	whereClause = "inventory.itemnum in ( " + itemList + ") and inventory.siteid = '" + siteID + "'"
	appName = "INVENTOR"
	reportName = "inventoryreport.rptdesign"  

	reportInfo = Hashtable();
	reportInfo.put("appname", appName);
	reportInfo.put("reportname", reportName);
	reportInfo.put("whereclause", whereClause);

	sessioncontext=service.webclientsession().getAdaptorInstance()
	webclientevent=WebClientEvent("RUNREPORTBYNAME", "mainrec", reportInfo, sessioncontext)
	sessioncontext.queueEvent(webclientevent)
