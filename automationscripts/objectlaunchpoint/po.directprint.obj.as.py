from java.util import Hashtable;
from psdi.util.logging import MXLoggerFactory
from psdi.server import MXServer
from psdi.webclient.system.controller import WebClientEvent

# report to be printed from application
appName = "RECEIPTS"   
reportName = "receiptitemreport.rptdesign" 
               	    
itemList = mbo.getString("PRINTLIST")

if len (itemList) > 0 :
    # remove first extra comma char to fit into report where clause
	itemList = itemList[1:]                 
	siteID = mbo.getString("SITEID")

	whrCls = "inventory.itemnum in ( " + itemList + ") and inventory.siteid = '" + siteID + "'" 

	reportInfo = Hashtable();
	reportInfo.put("appname", appName);
	reportInfo.put("reportname", reportName);
	reportInfo.put("whereclause", whrCls);

	sessioncontext=service.webclientsession().getAdaptorInstance()
	webclientevent=WebClientEvent("RUNREPORTBYNAME", "mainrec", reportInfo, sessioncontext)
	sessioncontext.queueEvent(webclientevent)
