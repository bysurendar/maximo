from psdi.mbo import MboConstants

def setError(errkey, errgrp, param):
  global errorkey, errorgroup, params
  errorkey = errkey
  errorgroup = errgrp
  params = param

fromWOMbo = False ;
hasOpenRecord = False ;
invUsageList = [] ;

def listToString(s):
	str1 = " ";
	return (str1.join(s)) ;

ownerMbo= mbo.getOwner() ;
if ownerMbo and ownerMbo.isBasedOn("WORKORDER"):
	fromWOMbo = True ;

if (mbo.getString("STATUS") == 'COMP') :
	wpMaterialSet = ownerMbo.getMboSet("WPMATERIAL") ;
	wpMaterialSet.setFlag(MboConstants.DISCARDABLE, True) ;
	if (wpMaterialSet.isEmpty() == False):
		wpMaterial = wpMaterialSet.moveFirst() ; 
		while (wpMaterial) : 
			if (wpMaterial.getMboSet("C_OPENINVUSE").isEmpty() == False) :
				hasOpenRecord = True ;
				invUsageList.append(wpMaterial.getMboSet("C_OPENINVUSE").getMbo(0).getString("INVUSENUM")) ;
			wpMaterial = wpMaterialSet.moveNext();

# remove duplicates from invUsageList
invUsageList = list(set(invUsageList))

def yes():
	service.log("Yes button is pressed, WO is completed") ;
		
def no():
	setError('AVOIDRECORD', 'WPINVUSE', [listToString(invUsageList)]) ;
	
def dflt():
	service.yncerror("WORKORDER", "CANAPPROVEWO",[listToString(invUsageList)]);
	
cases = {service.YNC_NULL:dflt, service.YNC_YES:yes, service.YNC_NO:no}

if (interactive and hasOpenRecord == True and fromWOMbo) :
	x = service.yncuserinput()
	cases[x]()
