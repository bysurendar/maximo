from java.util import HashMap
from com.ibm.json.java import JSON
from psdi.iface.router import HTTPHandler

vars = HashMap()
vars.put ("url","https://domain.okta.com/oauth2/ausindwieng345678/v1/token")
vars.put ("grantType","client_credentials")
vars.put ("clientId","0pb9l0p9hjBjU9YaY0j8")
vars.put ("clientSecret","sdIAUSzN1fuRxj3vd8ld9EQkZIeVQidMbBGTkWkk")

service.invokeScript("COMMON_LIB_GETTOKEN",vars)
#service.error('iface', vars.get("response"))
responseJSON = JSON.parse(vars.get("response"));

accessToken = ""

if responseJSON['access_token'] is None:
    service.error('iface', 'could_not_send')
else:
    #service.error('iface', responseJSON['access_token'])
    accessToken = responseJSON['access_token']
    

metaData = HashMap()
headers = HashMap()
headers.put('Authorization', 'Bearer %s' % accessToken)
metaData.put(HTTPHandler.HTTP_HEADERPROPS, headers)
urlProps = HashMap()
urlProps.put("limit","100")
urlProps.put("offset","0")
urlProps.put("siteid","BEDFORD")
metaData.put(HTTPHandler.HTTPGET_URLPROPS, urlProps)

response = service.invokeEndpoint("OAUTHTEST",metaData,"")
service.error('iface', response)

# use below code for MAS
#response = service.invokeEndpoint("OAUTHTEST",metaData,null)
#service.error('iface', response.toString())
