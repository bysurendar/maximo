from java.util import HashMap
from com.ibm.json.java import JSON
from psdi.iface.router import HTTPHandler
from com.ibm.json.java import JSONObject

metaData = HashMap()
headers = HashMap()
metaData.put(HTTPHandler.HTTP_HEADERPROPS, headers)
urlProps = HashMap()
urlProps.put("limit","5")
urlProps.put("offset","0")
metaData.put(HTTPHandler.HTTPGET_URLPROPS, urlProps)

response = service.invokeEndpoint("OAUTH",metaData,'')

obj = JSON.parse(response)
