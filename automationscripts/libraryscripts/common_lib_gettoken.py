from psdi.iface.router import HTTPHandler
from java.util import HashMap
from java.lang import String

# input variables are url,grantType, clientId, clientSecret
handler = HTTPHandler()
map = HashMap()
map.put(HTTPHandler.URL, url)
map.put("HTTPMETHOD", "POST")

# add header params
headers = HashMap()
headers.put('Content-Type', 'application/x-www-form-urlencoded')
map.put(HTTPHandler.HTTP_HEADERPROPS, headers)

# create form params
data = String('grant_type=%s&client_id=%s&client_secret=%s' % (grantType, clientId, clientSecret))
dataBytes = data.encode('utf-8')

responseBytes = handler.invoke(map, dataBytes)
# returns response to the calling script using invokeScript function
response = String(responseBytes, 'UTF-8')
