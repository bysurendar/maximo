from psdi.server import MXServer

polineSet = mbo.getMboSet("POLINE")

if (not polineSet.isEmpty() ):

    changeStatus = True
    
    polineMbo = polineSet.moveFirst()    
    while (polineMbo):
        coreStatus = polineMbo.getString("erppolinestatus")
        
        if (coreStatus != "POLINEOPENCUSTOMSTATUS"):
            changeStatus = False
            break
        polineMbo = polineSet.moveNext()
        
    
    if (changeStatus == True):
        mbo.changeStatus ("POCLOSECUSTOMSTATUS", MXServer.getMXServer().getDate(), "From Action")
