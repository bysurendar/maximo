from psdi.server import MXServer;
from java.sql import Statement;
from java.sql import Connection;
from java.sql import ResultSet;
from psdi.mbo import Mbo;


# Get an existing Database Connection from server
currentSet = service.getMboSet("WORKORDER",userInfo);
currentMbo = currentSet.getMbo(0);
conKey = userInfo.getConnectionKey()
con = currentMbo.getMboServer().getDBConnection(conKey);

checkSqlQuery = 'select a.system, max (b.transdate) from maximo.parent a , maximo.child b ' ;
checkSqlQuery += ' where a.parentid=b.ownerid and b.transdate > sysdate - 24 hours group by a.system order by a.system' ;

extSystemList = ['SYSTEM1','SYSTEM2','SYSTEM3','SYSTEM4'] 
receivedextSystemList = []
missingextSystemList = [] 

try: 
	s = con.createStatement(); 
	rs1 = s.executeQuery(checkSqlQuery);
	
	while(rs1.next()):
		extSystem = rs1.getString('system')
		receivedextSystemList.append(extSystem)
	rs1.close();

except	 Exception, e:
	service.log (e.printStackTrace())

finally:
	if s != "": 
		s.close();
	if con != 	"":
		currentMbo.getThisMboSet().getMboServer().freeDBConnection(conKey)
	if currentSet:
		currentSet.close()

if (len(receivedextSystemList) == 0 ):
    missingextSystemList = extSystemList
else:
	# difference between all subsystem list and received subsystem
	missingextSystemList = [item for item in extSystemList if item not in receivedextSystemList]

#service.error ("iface" , ''.join(missingList) )

if (len (missingextSystemList) > 0):
	# run logic if any subsystem missing cms messages from above checkSqlQuery
	
	# convert email string from system property and convert it into a List
	emailToString = MXServer.getMXServer().getSystemProperties().getProperty("custom.admin.emaillist")
	emailToList = emailToString.split(",")
	emailFrom = 'supportteam@company.com';
	emailSubject = 'Email Alert on Messages Missing for last 24 hours';
	emailBody = ["Dear Team, <br> <br>"]
	
	emailBody.append('<font face="verdana" color="red"> Messages Missing for last 24 hours from External systems: ')
	emailBody.append(','.join(missingextSystemList))
	emailBody.append(" </font> \n")

	currentSet = mxserver.getMboSet("WORKORDER",userInfo);
	currentMbo1 = currentSet.getMbo(0);
	conKey = userInfo.getConnectionKey()
	con1 = currentMbo.getMboServer().getDBConnection(conKey);

	getCMSListQuery = 'select a.system, max (b.transdate) from maximo.parent a , maximo.child b ' ;
	getCMSListQuery += ' where a.parentid=b.ownerid and b.transdate > sysdate - 24 hours group by a.system order by a.system ' ;
	
	try: 
		stmt = con1.createStatement(); 
		rs2 = stmt.executeQuery(getCMSListQuery);
			
		emailBody.append("<br><br><br>")
		emailBody.append("Please find all External Systems with Last Message Received Date for Analysis")
		emailBody.append("<br><br>")
		emailBody.append('<table border=1 style=width:1000px> <col width="5"> <col width="25">')
		emailBody.append("<thead> <tr> ")
		emailBody.append('<th bgcolor="slateblue"><font color="white">External System</font></th>')
		emailBody.append('<th bgcolor="slateblue"><font color="white">Last Received Date</font></th>')
		emailBody.append(" </tr> </thead>")
			
		while(rs2.next()):
			extSystem = rs2.getString('system')
			transDate = rs2.getString('2')
				
			emailBody.append("<tr>")
				
			emailBody.append("<td>")
			emailBody.append(extSystem)
			emailBody.append("</td>")
				
			emailBody.append("<td>")
			emailBody.append(transDate)
			emailBody.append("</td>")
				
			emailBody.append("</tr>")
				
		rs2.close();
			
		emailBody.append( " </table>")
			
		emailBody.append(" <br> <br>")
		emailBody.append(" Thanks and regards, <br>")
		emailBody.append(" Maximo Team <br>")
	
	except	 Exception, e1:
		service.log (e1.printStackTrace())
		
	finally:
		if stmt != "": 
			stmt.close();
		if con1 != 	"":
			currentMbo1.getThisMboSet().getMboServer().freeDBConnection(conKey)
		if currentSet:
		    currentSet.close()
	
	MXServer.sendEMail(emailToList,emailFrom, emailSubject, ''.join(emailBody));
