<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ibm="http://www.ibm.com/maximo" version="1.0" exclude-result-prefixes="ibm xsl">
 <xsl:output omit-xml-declaration="yes" indent="yes"/>
 <xsl:template match="@*|node()">
        <xsl:copy copy-namespaces="no">
            <xsl:apply-templates select="@*|node()" />
        </xsl:copy>
    </xsl:template>
	
	<!-- This block will find ASSET tags in payload and replace with RENAMEDASSET -->
    <xsl:template match="ibm:ASSET">
        <RENAMEDASSET><xsl:apply-templates  /></RENAMEDASSET>
    </xsl:template>

	<!-- This block will find Locations tags in payload and replace with RENAMEDLOCATION TAG
	without any prefix copied to renamed tags -->
	<xsl:template match="ibm:LOCATIONS">
    	<xsl:element name="RENAMEDLOCATIONTAG" namespace="http://www.ibm.com/maximo">
			<xsl:apply-templates select="@*|node()"/>
		</xsl:element>
	</xsl:template>	
	
	<!-- This block will remove default namespaces without prefix from transformed XML --> 
	<!-- Prefixed namespaces are removed by param exclude-result-prefixes in stylesheet tag -->
	<xsl:template match="*">
		<!-- remove element prefix -->
		<xsl:element name="{local-name()}">
			<xsl:apply-templates select="@*|node()" />
		</xsl:element>
	</xsl:template>
	<xsl:template match="@*">
		 <!-- remove attribute prefix -->
		<xsl:attribute name="{local-name()}">
			<xsl:value-of select="." />
		</xsl:attribute>
	</xsl:template>
		
</xsl:stylesheet>
