<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ibm="http://www.ibm.com/maximo" version="1.0">

<!-- Rename the PO tag into POINOTHERSYSTEM -->
<xsl:template match="ibm:PO">
    <xsl:element name="POINOTHERSYSTEM" namespace="http://www.ibm.com/maximo">
	<xsl:apply-templates select="@*|node()"/>
	</xsl:element>
</xsl:template>	

<xsl:template match="@*|node()">
    <xsl:copy>
        <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
</xsl:template>

  <!-- remove a tag -->
  <xsl:template match="ibm:REVISIONNUM" />
  
   <!-- remove blank line after deleting a tag -->
  <xsl:strip-space elements="*"/>

</xsl:stylesheet>