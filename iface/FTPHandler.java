/*
* Licensed Materials - Property of IBM
*
* Restricted Materials of IBM
*
* 5724-M19
*
* (C) COPYRIGHT IBM CORP. 2006. All Rights Reserved.
*
* US Government Users Restricted Rights - Use, duplication or
* disclosure restricted by GSA ADP Schedule Contract with IBM Corp
*
*/
package psdi.iface.samples;
import java.util.*;
import psdi.util.*;
import psdi.iface.util.*;
import psdi.iface.mic.*;
import psdi.iface.router.*;

/**
* This is a generic client for FTP .
* @author Anamitra.
*/
public class FTPHandler implements RouterHandler
{
	private Map ftpInfo = null;
	private EJBClient client = null;
	private EJBExit ejbExit = null;
	public static final String FTPURL = "FTPURL";
	public static final String USERNAME = "USERNAME";
	public static final String PASSWORD = "PASSWORD";
	private static List props = new ArrayList(7);
	
	static {
		props.add(new RouterPropsInfo(FTPURL));
		props.add(new RouterPropsInfo(USERNAME));
		props.add(new RouterPropsInfo(PASSWORD,true));
	}
	
	public void sendData(Map metaData, byte[] data, Map destinationMap) throws MXException {
	
	try {
		String destination = (String)metaData.get(MicConstants.MESSAGE_DESTINATION);
		String interfaceName = (String)metaData.get(MicConstants.MESSAGE_INTERFACE);
		
		if(destinationMap == null)
			throw new MXSystemException("iface", "mapping_information_not_found");
		this.ftpInfo = destinationMap;
		//code for FTP
		}
	
	catch (Exception e) {
		MicUtil.INTEGRATIONLOGGER.error(e);
		throw new MXSystemException("iface", "could_not_send");
		}
	}
	
	public List getProperties() {
		return props;
	}
	
	/**
	* @return FTPURL from the map.
	*/
	public String getFTPURL() {
		return (ftpInfo.get(FTPURL))== null ? null : ((String)ftpInfo.get(FTPURL)).trim();
	}
	
	/**
	* @return Returns the JNDI name of the EJB
	*/
	public String getUSERNAME() {
		return (ftpInfo.get(USERNAME))== null ? null : ((String)ftpInfo.get(USERNAME)).trim();
	}

	/**
	* @return Returns the password from the map.
	*/
	public String getPassword() {
		return (ftpInfo.get(PASSWORD))== null ? null : ((String)ftpInfo.get(PASSWORD)).trim();
	}
	
}