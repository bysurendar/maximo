# scripts runs from Inventory application
# mbo referred here is Inventory object
matUseSet = mbo.getMboSet("CUSTOMMATUSETRANSRELATIONSHIP")
isMboSelected = False
matUseTransID = []

from psdi.server import MXServer
sysdate = MXServer.getMXServer().getDate()

if matUseSet is not None:
	matUseMbo = matUseSet.moveFirst()
	while matUseMbo:
		if(matUseMbo is not None and matUseMbo.isSelected()):			
			matUseMbo.setValue("actualdate", sysdate, 11L)
			matUseMbo.setValue("transdate", sysdate, 11L)
			matUseTransID.append(matUseMbo.getInt("matusetransid"))
			isMboSelected = True
		matUseMbo = matUseSet.moveNext()
		
if isMboSelected == True:
	mbo.getThisMboSet().save()
	
userInfo = MXServer.getMXServer().getSystemUserInfo()
for i in matUseTransID:  
	whereClause = "matusetransid = " +  str(i) 		
	MXServer.getMXServer().lookup("MIC").exportData("PUBLISHCHANNELNAME","EXTERNALSYSTEMNAME", str(whereClause),userInfo, 1)