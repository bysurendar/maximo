# Object Structure Outbound Launch point to remove html tags from description

from psdi.util import HTML

def overrideValues (ctx):
    if ctx.getMboName() == "PR" or ctx.getMboName() == "PRLINE":
        ctx.overrideCol("DESCRIPTION_LONGDESCRIPTION", HTML.toPlainText(ctx.getMbo().getString("DESCRIPTION_LONGDESCRIPTION")))